(function(window, document)
{
    'use strict';
    var that;
    window.presentationJS = {
        deviceDetails : [],
        initialize : function()
        {
            that = this;

            const serverUrl ="https://api.evivve.com:"+localStorage.getItem('in_port');
            that.socket = io(serverUrl);
            that.getOns();
            //that.calamityMeter(1);
            that.j = 0;
            that.i = '';
            that.diff = '';
            that.first = '';
            that.second = '';
            that.third = '';
            that.fourth = '';
            that.name1 = '';
            that.name2 = '';
            that.name3 = '';
            that.name4 = '';
            //that.buyTile();
            //that.tribeMov();
            //that.showDashboard();
        },
        getOns : function()
        {
            that.socket.on("connected", function(data)
            {
                console.log(data);
                 $("#imgActive").show();
                 $("#imgInactive").hide();
                var params = {
                    "id" : data.id,
                    "type" : 'presentation'
                };
                that.emitContent("server-get-user-type", params)
            });
             that.socket.on('disconnect', function () {
                // Disconnected 
                $("#imgInactive").show();
                $("#imgActive").hide();
            });

               that.socket.on('error', function () {
                //  Error  
                $("#imgInactive").show();
                $("#imgActive").hide();
            });

             that.socket.on('connect_error', function () {
                // Connect Error  
                $("#imgInactive").show();
                $("#imgActive").hide();
            });

                that.socket.on('reconnect_error', function () {
                // Re Connect Error  
                $("#imgInactive").show();
                $("#imgActive").hide();
            });
  
  
            that.socket.on("send-user-to-presentation", that.showConnectedUser);
            that.socket.on("get-rooms", that.setRoom);
            that.socket.on("refresh-page-all", that.refreshPage);
            that.socket.on("refresh-shared-page", that.instantRefreshPage)
            that.socket.on("presentation-my-turn", that.changeTurns);
            that.socket.on("days-left", that.daysLefts);
            that.socket.on("strike-calamity", that.strikeCalamity);
            that.socket.on("commence-game", that.showDashboard);
            that.socket.on("initialize-data", that.setupGame);
            that.socket.on("tribe-value", that.setTribeValue);
            that.socket.on("production-quality", that.setProductQuality);
            that.socket.on("presentation-buy-tile", that.buyTile);
            that.socket.on("tribe-credits", that.tribeCredits);
            that.socket.on("tribe-mov", that.tribeMov);
            that.socket.on("average-week-and-cost", that.weekCost);
            that.socket.on("prepare-commence-game", that.commenceGame);
            that.socket.on("game-over", that.gameOver);
            that.socket.on("play-video", that.playVideo);
            that.socket.on("resume-game", that.resumeGame);
            that.socket.on("play-vr", that.playIntroVR);
            that.socket.on("badge-highlight", that.badgeHighLight);
            that.socket.on("lose-type", that.loseType);
        },
        days : 0,
        setupGame : function(data)
        {
            that.secDays = data['setup']['sec_days'];
            that.days = data['setup']['days'];
        },
        setRoom : function(data)
        {
            $('#please-wait').hide();
            $('#join-rrom').show();
            $('#roomName').html(data['room-name']);
            $('#gameName').html(data['room-name']);
            that.name1 = data['room-name'];
            that.pieChart();
            that.barChart();
        },
        setTribeValue : function(data)
        {
            console.log(data);
            $('#tribeValue').fadeOut(500, function()
            {
                $('#tribeValue').html(data['tribe-value']);
            }).fadeIn(500);
        },
        setProductQuality : function(data)
        {
            console.log(data);
            $('#productionQuality').fadeOut(500, function()
            {
                $('#productionQuality').html(Math.round(data['production-quality']));
            }).fadeIn(500);
        },
        lastAverageCost : 0,
        weekCost : function(data)
        {
            var averageCost = data['average-cost'];
            if(averageCost != null)
            {
                that.lastAverageCost = averageCost;
            }
            else
            {
                averageCost = that.lastAverageCost
            }
            var aw = Math.round(data['average-week'] / 7);
            $('#avgWeek').fadeOut(500, function()
            {
                $('#avgWeek').html(aw + ' weeks');
            }).fadeIn(500);
            $('#avgCost').fadeOut(500, function()
            {
                $('#avgCost').html(averageCost + '<span>cr</span>');
            }).fadeIn(500);
        },
        commenceGame : function()
        {
            $("#connected-user").html('Please wait while we allocate the land');
        },
        loseType : function(data)
        {
            console.log(data);
            if(data == "non-collab")
            {
                var audio = 'audio/NonCollab';
                $('#cal-audio').attr('src', audio + '.mp3');
            }
            else
            {
                var audio = 'audio/Collab';
                $('#cal-audio').attr('src', audio + '.mp3');
            }
        },
        gameOver : function(date)
        {
            if(date['result'] == 'won')
            {
                $('#video-cnt').attr('src', 'http://d2ow6rbiq7r6k4.cloudfront.net/videos/victory.mp4');
                $('.overlay').show(function()
                {
                    $('#video-cnt').get(0).play();
                });
                $('.video-close').click(function()
                {
                    $('.overlay').hide();
                    $('#video-cnt').get(0).pause();
                });
            }
            else
            {
                var audio = 'audio/MethaneGasRelease';
                $('#cal-audio').attr('src', audio + '.mp3');
                var card_board_number = 17;
                var card_board_interval = setInterval(function()
                {
                    card_board_number = card_board_number - 1;
                    if(card_board_number == 0)
                    {
                        clearInterval(card_board_interval);
                        document.getElementById("cal-audio").play();
                    }
                }, 1000);
            }
        },
        playVideo : function(data)
        {
            console.log(data);
            $('.overlay').show(function()
            {
                $('#video-cnt').get(0).play();
            });
            $('.video-close').click(function()
            {
                $('.overlay').hide();
                $('#video-cnt').get(0).pause();
            });
        },
        buyTile : function(data)
        {
            console.log(data);
            var count = data['tile-count'];
            var per = Math.floor(count / 2);
            //var per = 3;
            var odd = per % 2;
            var diff = '';
            var check = 0;
            //that.c1 = that.myPieChart.segments[2].value;
            //that.d1 = that.myPieChart.segments[3].value;

            if((25 + per) >= 67)
            {
                that.myPieChart.segments[0].value = 67;
            }
            else
            {
                that.myPieChart.segments[0].value = 25 + per;
                $('#gameScore').fadeOut(500, function()
                {
                    $('#gameScore').html(that.myPieChart.segments[0].value + '%');
                }).fadeIn(500);
            }
            var a1 = that.myPieChart.segments[0].value;
            var b1 = that.myPieChart.segments[1].value;
            var c1 = (100 - (a1 + b1)) / 2;

            console.log(c1);

            if(that.myPieChart.segments[2].value <= 0)
            {
                that.myPieChart.segments[2].value = 0;
            }
            else
            {
                that.myPieChart.segments[2].value = Math.ceil(c1);
                $('#ranScore').fadeOut(500, function()
                {
                    $('#ranScore').html(that.myPieChart.segments[2].value + '%');
                }).fadeIn(500);
            }

            if(that.myPieChart.segments[3].value <= 0)
            {
                that.myPieChart.segments[3].value = 0;
            }
            else
            {
                that.myPieChart.segments[3].value = Math.floor(c1);
                $('#siegScore').fadeOut(500, function()
                {
                    $('#siegScore').html(that.myPieChart.segments[3].value + '%');
                }).fadeIn(500);
            }

            that.myPieChart.update();
            console.log(odd);
            console.log(diff);
            console.log(per);
        },
        tribeCredits : function(data)
        {
            console.log(data);
            $('#tribeCredits').fadeOut(500, function()
            {
                $('#tribeCredits').html(data['tribe-credits'] + '<span>cr</span>');
            }).fadeIn(500);
            //console.log(that.myBarChart.datasets[0]);
        },
        tribeMov : function(data)
        {
            console.log(data);
            $('#mv').fadeOut(500, function()
            {
                $('#mv').html(data['tribe-mov']);
            }).fadeIn(500);
            that.myBarChart.datasets[0].bars[0].value = data['percentage'];
            that.myBarChart.update();
            var ev = Math.floor(data['percentage'] / 10);
            $('#ev_points').fadeOut(500, function()
            {
                $('#ev_points').html(ev);
            }).fadeIn(500);
        },
        showDashboard : function(data)
        {
            $(".join-rrom").hide();
            $(".main").show();
        },
        emitContent : function(key, object)
        {
            that.socket.emit(key, object);
        },
        showConnectedUser : function(data)
        {
            console.log(data);
            if(data != null)
            {
                that.deviceDetails.push(data);
                $("#connected-user").append("<div>" + data['name'] + " connected</div>")
            }
        },
        refreshPage : function()
        {
            console.log("refresh");
            setTimeout(function()
            {
                window.location.reload();
            }, 1000);
        },
        instantRefreshPage : function()
        {
            window.location.reload();
        },
        changeTurns : function(data)
        {
            console.log(data);
            $("#waiting-user").html("Current Turn");
            var name = data['profile']['name'];
            $("#connected-user").html(name);
        },
        daysLefts : function(data)
        {
            that.daysLeft = data['days_left'];
            $('#days-left').fadeOut(500, function()
            {
                $("#days-left").html(data['days_left']);
            }).fadeIn(500);
            if(that.j == 0)
            {
                that.i = that.daysLeft;
                that.j = 1;
                that.diff = that.i / 4;
                that.first = Math.round(that.i - that.diff);
                that.second = Math.round(that.first - that.diff);
                that.third = Math.round(that.second - that.diff);
                that.fourth = 0;
            }
            console.log(that.i);
            console.log(that.diff);
            console.log(that.first);
            console.log(that.second);
            console.log(that.third);
            console.log(that.fourth);
            console.log(that.daysLeft);
            //console.log(that.myBarChart.datasets[0]);
            if(that.daysLeft == that.first)
            {
                if(that.myPieChart.segments[1].value >= 33)
                {
                    that.myPieChart.segments[1].value = 33;
                }
                else
                {
                    that.myPieChart.segments[1].value += 2;
                    $('#nemScore').fadeOut(500, function()
                    {
                        $('#nemScore').html(that.myPieChart.segments[1].value + '%');
                    }).fadeIn(500);
                }

                var a1 = that.myPieChart.segments[0].value;
                var b1 = that.myPieChart.segments[1].value;
                var c1 = (100 - (a1 + b1)) / 2;

                console.log(c1);
                if(that.myPieChart.segments[2].value <= 0)
                {
                    that.myPieChart.segments[2].value = 0;
                }
                else
                {
                    that.myPieChart.segments[2].value = Math.ceil(c1);
                    $('#ranScore').fadeOut(500, function()
                    {
                        $('#ranScore').html(that.myPieChart.segments[2].value + '%');
                    }).fadeIn(500);
                }

                if(that.myPieChart.segments[3].value <= 0)
                {
                    that.myPieChart.segments[3].value = 0;
                }
                else
                {
                    that.myPieChart.segments[3].value = Math.floor(c1);
                    $('#siegScore').fadeOut(500, function()
                    {
                        $('#siegScore').html(that.myPieChart.segments[3].value + '%');
                    }).fadeIn(500);
                }
                that.myPieChart.update();
                that.myBarChart.datasets[0].bars[1].value += 20;
                that.myBarChart.datasets[0].bars[2].value += 10;
                that.myBarChart.datasets[0].bars[3].value += 5;
                that.myBarChart.update();
            }
            else if(that.daysLeft == that.second)
            {
                if(that.myPieChart.segments[1].value >= 33)
                {
                    that.myPieChart.segments[1].value = 33;
                }
                else
                {
                    that.myPieChart.segments[1].value += 2;
                    $('#nemScore').fadeOut(500, function()
                    {
                        $('#nemScore').html(that.myPieChart.segments[1].value + '%');
                    }).fadeIn(500);
                }

                var a1 = that.myPieChart.segments[0].value;
                var b1 = that.myPieChart.segments[1].value;
                var c1 = (100 - (a1 + b1)) / 2;

                console.log(c1);
                if(that.myPieChart.segments[2].value <= 0)
                {
                    that.myPieChart.segments[2].value = 0;
                }
                else
                {
                    that.myPieChart.segments[2].value = Math.ceil(c1);
                    $('#ranScore').fadeOut(500, function()
                    {
                        $('#ranScore').html(that.myPieChart.segments[2].value + '%');
                    }).fadeIn(500);
                }

                if(that.myPieChart.segments[3].value <= 0)
                {
                    that.myPieChart.segments[3].value = 0;
                }
                else
                {
                    that.myPieChart.segments[3].value = Math.floor(c1);
                    $('#siegScore').fadeOut(500, function()
                    {
                        $('#siegScore').html(that.myPieChart.segments[3].value + '%');
                    }).fadeIn(500);
                }
                that.myPieChart.update();
                that.myBarChart.datasets[0].bars[1].value += 20;
                that.myBarChart.datasets[0].bars[2].value += 10;
                that.myBarChart.datasets[0].bars[3].value += 5;
                that.myBarChart.update();
            }
            else if(that.daysLeft == that.third)
            {
                if(that.myPieChart.segments[1].value >= 33)
                {
                    that.myPieChart.segments[1].value = 33;
                }
                else
                {
                    that.myPieChart.segments[1].value += 2;
                    $('#nemScore').fadeOut(500, function()
                    {
                        $('#nemScore').html(that.myPieChart.segments[1].value + '%');
                    }).fadeIn(500);
                }

                var a1 = that.myPieChart.segments[0].value;
                var b1 = that.myPieChart.segments[1].value;
                var c1 = (100 - (a1 + b1)) / 2;

                console.log(c1);
                if(that.myPieChart.segments[2].value <= 0)
                {
                    that.myPieChart.segments[2].value = 0;
                }
                else
                {
                    that.myPieChart.segments[2].value = Math.ceil(c1);
                    $('#ranScore').fadeOut(500, function()
                    {
                        $('#ranScore').html(that.myPieChart.segments[2].value + '%');
                    }).fadeIn(500);
                }

                if(that.myPieChart.segments[3].value <= 0)
                {
                    that.myPieChart.segments[3].value = 0;
                }
                else
                {
                    that.myPieChart.segments[3].value = Math.floor(c1);
                    $('#siegScore').fadeOut(500, function()
                    {
                        $('#siegScore').html(that.myPieChart.segments[3].value + '%');
                    }).fadeIn(500);
                }

                that.myPieChart.update();
                that.myBarChart.datasets[0].bars[1].value += 20;
                that.myBarChart.datasets[0].bars[2].value += 10;
                that.myBarChart.datasets[0].bars[3].value += 5;
                that.myBarChart.update();
            }
            else if(that.daysLeft == that.fourth)
            {
                if(that.myPieChart.segments[1].value >= 33)
                {
                    that.myPieChart.segments[1].value = 33;
                }
                else
                {
                    that.myPieChart.segments[1].value += 2;
                    $('#nemScore').fadeOut(500, function()
                    {
                        $('#nemScore').html(that.myPieChart.segments[1].value + '%');
                    }).fadeIn(500);
                }

                var a1 = that.myPieChart.segments[0].value;
                var b1 = that.myPieChart.segments[1].value;
                var c1 = (100 - (a1 + b1)) / 2;

                console.log(c1);
                if(that.myPieChart.segments[2].value <= 0)
                {
                    that.myPieChart.segments[2].value = 0;
                }
                else
                {
                    that.myPieChart.segments[2].value = Math.ceil(c1);
                    $('#ranScore').fadeOut(500, function()
                    {
                        $('#ranScore').html(that.myPieChart.segments[2].value + '%');
                    }).fadeIn(500);
                }

                if(that.myPieChart.segments[3].value <= 0)
                {
                    that.myPieChart.segments[3].value = 0;
                }
                else
                {
                    that.myPieChart.segments[3].value = Math.floor(c1);
                    $('#siegScore').fadeOut(500, function()
                    {
                        $('#siegScore').html(that.myPieChart.segments[3].value + '%');
                    }).fadeIn(500);
                }
                that.myPieChart.update();
                that.myBarChart.datasets[0].bars[1].value += 20;
                that.myBarChart.datasets[0].bars[2].value += 10;
                that.myBarChart.datasets[0].bars[3].value += 5;
                that.myBarChart.update();
            }
        },
        resumeGame : function(data)
        {
            that.calamityMeter(that.calamityData);
        },
        strikeCalamity : function(data)
        {
            var audio = 'audio/' + data['calamity']['calamity'].replace(' ', '_').toLocaleLowerCase();
            $('#cal-audio').attr('src', audio + '.mp3');
            var card_board_number = 17;
            var card_board_interval = setInterval(function()
            {
                card_board_number = card_board_number - 1;
                if(card_board_number == 0)
                {
                    clearInterval(card_board_interval);
                    document.getElementById("cal-audio").play();
                }
            }, 1000);

            console.log(data);
            $('.bar-div').css('width', '100%');
            //$("#calamity").html(data['calamity']['calamity'] + " at level : " + data['calamity']['level']);
            $('.footer').show();
            var weeks = '';
            if(data['calamity']['level'] == 'Level1')
            {
                weeks = '2 weeks';
            }
            else if(data['calamity']['level'] == 'Level2')
            {
                weeks = '4 weeks';
            }
            else if(data['calamity']['level'] == 'Level3')
            {
                weeks = '8 weeks';
            }

            that.calamityData = data['calamity']['level'];
            $('.footer-text').html(data['calamity']['calamity'] + ' - ' + weeks);
            (function blink()
            {
                $('.footer-text').fadeOut(500).fadeIn(500, blink);
            })();
            $('#resourceAvail').fadeOut(500, function()
            {
                $('#resourceAvail').html('89%');
            }).fadeIn(500);

        },
        secDays : 0,
        calamityMeter : function(lvl)
        {
            var days = '';
            if(lvl == "Level1")
            {
                days = 7 * 2 * that.secDays;
            }
            else if(lvl == "Level2")
            {
                days = 7 * 4 * that.secDays;
            }
            else if(lvl == "Level3")
            {
                days = 7 * 8 * that.secDays;
            }
            var remaining = days;
            console.log(days);
            var interval = setInterval(function()
            {
                remaining--;
                var per = Math.round((remaining / days) * 100);
                console.log(per);
                $('.bar-div').css('width', per + '%');
                if(remaining <= 0)
                {
                    $('#resourceAvail').html('100%');
                    $('.footer').hide();
                    clearInterval(interval);
                }
            }, 1000);
        },
        pieChart : function()
        {
            var ctx = document.getElementById("pieChart").getContext("2d");
            var data = [
                {
                    value : 25,
                    color : "#FCA600",
                    highlight : "#FCA600",
                    label : that.name1
                },
                {
                    value : 25,
                    color : "#DE6D00",
                    highlight : "#DE6D00",
                    label : "Nemesis"
                },
                {
                    value : 25,
                    color : "#D95100",
                    highlight : "#D95100",
                    label : "Ranger"
                },
                {
                    value : 25,
                    color : "#D23600",
                    highlight : "#D23600",
                    label : "Siegfried"
                }
            ];
            that.myPieChart = new Chart(ctx).Pie(data, {
                animationEasing : 'linear',
                showTooltips : true,
                scaleShowLabels : true,
                segmentShowStroke : false,
            });
            console.log(that.myPieChart.segments[0].value);
            console.log(that.myPieChart.segments[0]);
        },
        barChart : function()
        {
            var ctx = document.getElementById("barChart").getContext("2d");
            var data = {
                labels : [that.name1, "Nemesis", "Ranger", "Siegfried"],
                datasets : [
                    {
                        label : "My First dataset",
                        //fillColor : "rgba(255,255,255,1)",
                        //strokeColor : "rgba(255,255,255,1)",
                        //highlightFill : "rgba(255,255,255,1)",
                        //highlightStroke : "rgba(255,255,255,1)",
                        data : [0, 0, 0, 0]
                    }
                ]
            };
            that.myBarChart = new Chart(ctx).Bar(data, {
                barValueSpacing : 10,
                scaleFontColor : "#ffffff"
            });

            that.myBarChart.datasets[0].bars[0].fillColor = "#FCA600"; //bar 1
            that.myBarChart.datasets[0].bars[1].fillColor = "#DE6D00"; //bar 2
            that.myBarChart.datasets[0].bars[2].fillColor = "#D95100"; //bar 3
            that.myBarChart.datasets[0].bars[3].fillColor = "#D23600"; //bar 4
            that.myBarChart.update();
        },
        blink : function()
        {
            $('.blink_me').fadeOut(500).fadeIn(500);
        },
        playIntroVR : function()
        {
            var audio = 'audio/IntroNarrative';
            $('#cal-audio').attr('src', audio + '.mp3');
            var card_board_number = 17;
            var card_board_interval = setInterval(function()
            {
                card_board_number = card_board_number - 1;
                if(card_board_number == 0)
                {
                    clearInterval(card_board_interval);
                    document.getElementById("cal-audio").play();
                }
            }, 1000);
        },
        badgeHighLight : function(data)
        {
            if(data['flag'] == "on")
            {
                $(".main-btn-div .btn").css({
                    "display" : 'none'
                });
                $("#" + data['type']).css({
                    "display" : 'inline-block'
                });
            }
            else
            {
                $("#" + data['type']).css({
                    "display" : 'none'
                });
            }
        }
    };
    window.onload = function()
    {
        presentationJS.initialize();
    }
})(window, document);