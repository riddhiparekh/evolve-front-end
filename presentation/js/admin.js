(function (window, document) {
    var that;
    const host = window.location.hostname=="dev.evivve.com" ? "https://api.evivve.com":"http://localhost";
    // const serverUrl ="http://" +localStorage.getItem('st_ip')+ ":"+localStorage.getItem('in_port');
    const serverUrl ="https://api.evivve.com:"+localStorage.getItem('in_port');
    console.log(serverUrl);


    window.adminJS = {
        initialize: function () {
            var myVideo = document.getElementById('video1');

            that = this;
            that.socket = io(serverUrl);
            that.getOns();
            $("#btn-game").on('click', that.createGame);
            $("#btn-start-game").on('click', that.startGame);
            $("#btn-commence-game").on('click', that.commenceGame);
            $("#btn-vr-game").on('click', that.playVR);
            $("#btn-refresh-game").on('click', that.refreshPage);
            $("#_logoutButton").on('click', that.logout);
            $("#btn-col").on('click', that.collaborativeVideo);
            $("#btn-non-col").on('click', that.nonCollaborativeVideo);
            $("#btn-pause-game").on('click', that.pauseGame);
            $(".btn-badges").on('click', that.toggleBadge);
            $('#btn-open-cms').click(that.openCms);
            $('#btn-open-shared').click(that.openShared);
            $('#btn-play-video').click(that.playVideo);
            $('#btn-close-video').click(that.closeVideo);

        },
        id: null,
        getOns: function () {
            that.socket.on("connected", function (data) {
                console.log(data);
                $("#imgActive").show();
                 $("#imgInactive").hide();
                var params = {
                    "id": data.id,
                    "type": 'admin'
                };
                that.emitContent("server-get-user-type", params);
            });
            
            that.socket.on('disconnect', function () {
                // Disconnected 
                $("#imgInactive").show();
                $("#imgActive").hide();
            });

             that.socket.on('error', function () {
                //  Error  
                $("#imgInactive").show();
                $("#imgActive").hide();
            });

             that.socket.on('connect_error', function () {
                // Connect Error  
                $("#imgInactive").show();
                $("#imgActive").hide();
            });

                that.socket.on('reconnect_error', function () {
                // Re Connect Error  
                $("#imgInactive").show();
                $("#imgActive").hide();
            });
  
            that.socket.on("prepare-commence-game", that.prepareCommenceGame);
            that.socket.on("game-over", that.gameOver);
            that.socket.on("strike-calamity", that.calamityStriked);
            that.socket.on("resume-game", that.resumeGameAfterCalamity);
            that.socket.on('initialize-data', that.setUp);
        },
        lastCalamity: {},
        setUp: function (data) {
            that.setupData = data['setup'];
            that.credits = that.setupData['credits'];
            that.days = that.setupData['days'];
            that.sec_days = that.setupData['sec_days'];
        },
        calamityStriked: function (data) {
            that.lastCalamity = data['calamity'];
            $('#btn-pause-game').hide();
            $('#badges-div').hide();
        },
        calamityLevel: {
            "Level1": {
                "weeks": 2,
                "dev-points": 2
            },
            "Level2": {
                "weeks": 4,
                "dev-points": 4
            },
            "Level3": {
                "weeks": 8,
                "dev-points": 6
            }
        },
        resumeGameAfterCalamity: function (data) {
            $('#badges-div').show();
            if (data['flag'] == 1) {
                $('#btn-pause-game').show();
            }
            else {
                var seconds = (that.calamityLevel[that.lastCalamity['level']]['weeks'] * 7 * that.sec_days) * 1000;
                setTimeout(function () {
                    $('#btn-pause-game').show();
                }, seconds);
            }
        },
        pauseGame: function () {
            $('#btn-pause-game').hide();
            that.emitContent("server-pause-game", {});
        },
        collaborativeVideo: function () {
            that.emitContent("server-lose-video-type", "collab");
        },
        nonCollaborativeVideo: function () {
            that.emitContent("server-lose-video-type", "non-collab");
        },
        gameOver: function (data) {
            if (data['result'] != "won") {
                $("#video-div").show();
            }
        },
        emitContent: function (key, object) {
            that.socket.emit(key, object);
        },
        createGame: function () {
            var params = {
                'game-name': $("#txt_instance").val()
            };
            that.emitContent('server-create-game', params);
            $("#btn-game").fadeOut();
            $("#txt_instance").fadeOut();
            $("#btn-start-game").fadeIn();
            $("#dd_round").fadeIn();
            //$('.create-game').hide();
            $('.start-game').show();
        },
        startGame: function () {
            var params = {
                "round": $("#dd_round").val()
            };
            that.emitContent("start-the-game", params);
        },
        refreshPage: function () {
            $("#loader").show();
                // Todo:Change This URL Docker Application
                const refreshUrl = host+":3000/api/restartApp";

                axios.post(refreshUrl,{code:code})
                .then(function (resp) {

                    console.log(resp);
                    if(resp.data.code==200)
                    {

                            localStorage.setItem('st_code', resp.data.data.st_code);
                            localStorage.setItem('in_port', resp.data.data.in_port);
                            localStorage.setItem('st_ip', resp.data.data.st_ip);

                            $.toast({
                                heading: 'Success',
                                text: 'An App has been successfully restarted.',
                                showHideTransition: 'slide',
                                icon: 'success'
                            });
                            
                               $("#loader").hide();

                            that.emitContent("refresh-page", {});
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);

                         
                    }
        

                })
                .catch(function (error) {
                    console.log(error.response)
                     
                      $.toast({
                        heading: 'Error',
                        text: error.response.data.data,
                        showHideTransition: 'slide',
                        icon: 'error'
                    });
                });

           
        },
        openCms: function () {
            window.open(
                window.location.origin + ":3000/",
                '_blank'
            );
        },
        openShared: function () {
            window.open(
                window.location.origin + "/shared/index.html",
                '_blank'
            );
        },
        logout: function () {

                // TODO : Change the URL

                axios.post(host+":3000/api/logout",{code:code})
                .then(function (response) {

                            axios.get(serverUrl+'/logout')
                            .then(function (response) {
                            console.log(response);

                            })
                            .catch(function (error) {
                            console.log(error);
                            });


                          setTimeout(function () {
                            localStorage.removeItem('token');
                            window.location.reload();
                        }, 2000);

                })
                .catch(function (error) {
                    console.log(error);
                });


              

           
        },
        prepareCommenceGame: function () {
            console.log("prepare");
            $("#btn-commence-game").fadeIn();
            $("#btn-start-game").fadeOut();
            $("#dd_round").fadeOut();
            $('.commence-game').show();
            $('.start-game').hide();
        },
        commenceGame: function () {
            that.emitContent("server-commence-game", {});
            $('#btn-commence-game').hide();
            $('#btn-pause-game').show();
            $('#badges-div').show();
        },
        executeMethod: function (param) {
            var params = {
                "function": param
            };
            that.emitContent("execute-method", params);
        },

        playVideo: function () {
            that.emitContent("server-play-video", {});
        },
        resumeGame: function () {
            console.log('resume');
            var params = {};
            that.emitContent("server-resume-game-manually", params);
        },
        toggleBadge: function () {
            var isSelected = $(this).attr("data-is-selected");
            var type = $(this).attr("data-type");
            var params = {};
            if (isSelected == 0) {
                var that1 = this;
                $(".btn-badges").attr("data-is-selected", 0).css({
                    "border-color": "#7B8292"
                });

                setTimeout(function () {
                    $(that1).attr("data-is-selected", 1).css({
                        "border-color": "#00FFF7"
                    });
                });

                params = {
                    "type": type,
                    "flag": "on"
                }
            }
            else {
                $(this).attr("data-is-selected", 0).css({
                    "border-color": "#7B8292"
                });
                params = {
                    "type": type,
                    "flag": "off"
                }
            }

            that.emitContent("server-badge-highlight", params);
        }
    };
    window.onload = function () {
        adminJS.initialize();
    }
})(window, document);