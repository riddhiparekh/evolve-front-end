(function(window, document)
{
    'use strict';
    var that;
    window.presentationJS = {
        deviceDetails : [],
        initialize : function()
        {
            that = this;
            const serverUrl ="http://" +localStorage.getItem('st_ip')+ ":"+localStorage.getItem('in_port');
            that.socket = io(serverUrl);
            that.getOns();
        },
        getOns : function()
        {
            that.socket.on("connected", function(data)
            {
                console.log(data);
                var params = {
                    "id" : data.id,
                    "type" : 'presentation'
                };
                that.emitContent("server-get-user-type", params)
            });
            that.socket.on("send-user-to-presentation", that.showConnectedUser);
            that.socket.on("refresh-page-all", that.refreshPage);
            that.socket.on("presentation-my-turn", that.changeTurns);
            that.socket.on("days-left", that.daysLeft);
            that.socket.on("strike-calamity", that.strikeCalamity);
        },
        emitContent : function(key, object)
        {
            that.socket.emit(key, object);
        },
        showConnectedUser : function(data)
        {
            console.log(data);
            if(data != null)
            {
                that.deviceDetails.push(data);
                $("#connected-user").append("<div>" + data['name'] + " connected</div>")
            }
        },
        refreshPage : function()
        {
            console.log("refresh");
            setTimeout(function()
            {
                window.location.reload();
            }, 1000);
        },
        changeTurns : function(data)
        {
            console.log(data);
            var name = data['profile']['name'];
            $("#connected-user").html(name + "s turn");
        },
        daysLeft : function(data)
        {
            var daysLeft = data['days_left'];
            $("#days-left").html(daysLeft + " Days Left")
        },
        strikeCalamity : function(data)
        {
            console.log(data);
            $("#calamity").html(data['calamity']['calamity'] + " at level : " + data['calamity']['level']);
        }
    };
    window.onload = function()
    {
        presentationJS.initialize();
    }
})(window, document);