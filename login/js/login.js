// Todo:Change this to docker APP
const host = window.location.hostname=="dev.evivve.com" ? "https://api.evivve.com":"http://localhost";
const serverUrl = host+":3000/api/otherusers/login";
console.log(serverUrl)


openAdmin = function() {
    window.open(
        window.location.origin + "/presentation/admin.html",
        '_self'
    );
}

openShared = function (){
    window.open(
        window.location.origin + "/shared/",
        '_blank'
    );
};

if (localStorage.getItem('token')) {
    // openShared();
    $.toast({
        heading: 'Success',
        text: 'Alreday LoggedIn! Redirecting you to admin Page..',
        showHideTransition: 'slide',
        icon: 'success'
    });
    setTimeout(function() {
        openAdmin();
    }, 2000);
} else {
    $('#_loading').addClass('hide');
    $('#_login-form').removeClass('hide');
}

$("#_logoutButton").click(function () {
    localStorage.removeItem('token');
    window.location.reload();
    axios.get(window.location.origin+':'+data.data.in_port+'/restart')
        .then(function (response) {
                console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });
});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

$(".login-form").submit(function (event) {

    // Stop form from submitting normally
    event.preventDefault();
    $("#loader").show();
    $("#btnLogin").attr('disabled','disabled');

    const email = $('#_email').val();
    if (!isEmail(email)) {
          $("#loader").hide();
          $("#btnLogin").attr('disabled','');
        $.toast({
            heading: 'Error',
            text: "Enter valid email address!",
            showHideTransition: 'slide',
            icon: 'error'
        });
        return;
    }
    const password = $('#_password').val();
    $.post(serverUrl, {
        email: email,
        password: password
    }).done(function (data) {

        console.log(data);
       
        
        if (data.data.token) {
           
            axios.get("https://api.evivve.com:"+data.data.in_port+'/restart')
                .then(function (response) {
                        console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });

                setTimeout(function() {
                    localStorage.setItem('st_code', data.data.st_code);
                    localStorage.setItem('in_port', data.data.in_port);
                    localStorage.setItem('st_ip', data.data.st_ip);
                    localStorage.setItem('token', data.data.token);
                    // openShared();
                    $("#loader").hide();
                    $("#btnLogin").attr('disabled','');
                    $.toast({
                        heading: 'Success',
                        text: 'Successfully LoggedIn! Redirecting you to admin Page..',
                        showHideTransition: 'slide',
                        icon: 'success'
                    });
                    setTimeout(function() {

                        openAdmin();
                    }, 2000);
                }, 3000);
        }
    }, 'json').fail(function (error) {
        $("#loader").hide();
        $("#btnLogin").attr('disabled','');
        $.toast({
            heading: 'Error',
            text: error.responseJSON.data,
            showHideTransition: 'slide',
            icon: 'error'
        });
    }, 'json');
});

$(window).focus(function () {
    //window.location.reload();
});